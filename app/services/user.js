import config from '../config'
import { authHeader, authHeaderFile } from '../helpers/authHeader'
import { handleResponse } from './main'

export const login = (username, password) => {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ username, password })
  }

  return fetch(`${config.apiUrl}/login`, requestOptions)
  .then(handleResponse)
  .then(response => {
    // login successful if there's a jwt token in the response
    if (response) {
      // store user details and jwt token in local storage to keep user logged in between page refreshes
      localStorage.setItem('user', JSON.stringify(response.data.user))
      localStorage.setItem('token', JSON.stringify(response.data.token))
      console.log('token')
    }
    return response.data.user
  })
}

export const logout = () => {
  const requestOptions = {
    method: 'POST',
    headers: authHeader()
  }

  return fetch(`${config.apiUrl}/logout`, requestOptions).then(handleResponse)
}
