import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux'
import { AppContainer } from 'react-hot-loader';
import { App } from './components/App'
import { configureStore, history } from './store/configureStore';
import './app.global.css';

const store = configureStore();

render(
  <Provider store={store}>
    <App history={history}/>
  </Provider>,
  document.getElementById('root')
);

if (module.hot) {
  module.hot.accept('./components/App', () => {
    // eslint-disable-next-line global-require
    const NextRoot = require('./components/App').default;
    render(
      <Provider store={store}>
        <App history={history}/>
      </Provider>,
      document.getElementById('root')
    );
  });
}
