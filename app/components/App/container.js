import App from './App'
import { connect } from 'react-redux'

const mapStateToProps = state => {
  return {}
}
const mapDispatchToProps = dispatch => {
  return {}
}

const connectedApp = connect(mapStateToProps, mapDispatchToProps)(App)
export { connectedApp as App }
