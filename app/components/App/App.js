import React, {Component} from 'react'
import { Switch, Router, Route } from 'react-router-dom'
import { PrivateRoute } from '../PrivateRoute'
import { Dashboard } from '../Dashboard'
import { Login } from '../Login'

export default class App extends Component {
  render () {
    return (
      <div className='content'>
        <Router history={this.props.history}>
          <Switch>
            <Route path='/login' component={Login} />
            <PrivateRoute path='/' component={Dashboard} />
          </Switch>
        </Router>
      </div>
    )
  }
}
