import Login from './Login'
import { connect } from 'react-redux'
import { login } from '../../actions/user'

function mapStateToProps (state) {
  const { loggingIn } = state.authentication
  return {
    loggingIn
  }
}
const mapDispatchToProps = dispatch => {
  return {
    login (...args) {
      return dispatch(login(...args))
    }
  }
}
const connectedLogin = connect(mapStateToProps, mapDispatchToProps)(Login)
export { connectedLogin as Login }
