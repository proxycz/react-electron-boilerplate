import React from 'react'
import { history } from '../../store/configureStore'
import { Button, Form, Icon, Segment, Header, Input, Message } from 'semantic-ui-react'

export default class Login extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      username: '',
      password: '',
      submitted: false,
      error:false,
      loggingIn: false
    }
  }

  render () {
    let error = false
    const handleSubmit = (e) => {
      e.preventDefault()
      this.setState({ submitted: true, loggingIn: true })
      const { login } = this.props
      const { username, password } = this.state
      if (username && password) {
        this.setState({ error: false })
          login(username, password).then(() =>
            history.push('/')
          )
      }else{
        this.setState({ error: true , loggingIn: false})
      }
    }
    const handleChange = (e) => {
      const { name, value } = e.target
      this.setState({ [name]: value })
    }
    const { username, password, submitted } = this.state
    return (
      <div className='login'>
        <Segment compact>
          <Header as='h2'>Login</Header>
          <Form onSubmit={handleSubmit} error={this.state.error}>
            <Form.Field>
              <Input type='email' name='username' value={username} placeholder='Email' onChange={handleChange} />
              <Message
                error
                content='Username is required'
              />
            </Form.Field>
            <Form.Field>
              <Input type='password' name='password' value={password} placeholder='Password' onChange={handleChange} />
              <Message
                error
                content='Password is required'
              />
            </Form.Field>
            { !this.state.loggingIn ?
              <Button positive animated>
                <Button.Content visible>Log In</Button.Content>
                <Button.Content hidden>
                  <Icon name='arrow right' />
                </Button.Content>
              </Button> :
              <Button loading>
               Loading
             </Button>
         }
          </Form>
        </Segment>
      </div>
    )
  }
}
