import * as userService from '../services/user'
import { error as errorAlert, success as successAlert } from './alert'
import { GET_USER_REQUEST, GETALL_REQUEST,GETALL_SUCCESS,GETALL_FAILURE, GET_USER,GET_USER_SUCCESS,GET_USER_FAILURE,GET_MY_PROFILE,EMPTY_USER, LOGOUT, CREATE_USER_SUCCESS, LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE }from '../constants/user'
import { history } from '../store/configureStore'

export const login = (username, password) => {
  return dispatch => {
    dispatch(request({ username }))
    return userService.login(username, password)
    .then(
      response => {
        dispatch(success(response))
        return true
      },
      error => {
        dispatch(failure(error.toString()))
        dispatch(errorAlert(error.toString()))
      }
    )
  }
  function request (user) { return { type: LOGIN_REQUEST, user } }
  function success (user) { return { type: LOGIN_SUCCESS, user } }
  function failure (error) { return { type: LOGIN_FAILURE, error } }
}

export const logout = () => {
  return dispatch => {
    userService.logout()
    .then(
      response => {
        localStorage.removeItem('token')
        localStorage.removeItem('user')
        history.push('/login')
        dispatch(success(response))
      },
      error => dispatch(errorAlert(error.toString()))
    )
  }
  function success (user) { return { type: LOGOUT } }
}
export const getAll = (showLoading = true) => {
  return dispatch => {
    if (showLoading) {
      dispatch(request())
    }

    userService.getAll()
    .then(
      response => dispatch(success(response.data)),
      error => dispatch(failure(error.toString()))
    )
  }

  function request () { return { type: GETALL_REQUEST } }
  function success (users) { return { type: GETALL_SUCCESS, users } }
  function failure (error) { return { type: GETALL_FAILURE, error } }
}

export const createUser = (user) => {
  return dispatch => {
    return userService.createUser(user)
    .then(
      response => {
        dispatch(success(response.data))
        dispatch(successAlert(response.message))
        return true
      },
      error => dispatch(failure(error.toString()))
    )
  }

  function success (user) { return { type: CREATE_USER_SUCCESS, user } }
  function failure (error) { return { type: CREATE_USER_FAILURE, error } }
}
export const getUser = (id) => {
  return dispatch => {
    dispatch(request())

    return userService.getUser(id)
    .then(
      response => {
        dispatch(success(response.data))
        return true
      },
      error => dispatch(failure(error.toString()))
    )
  }

  function request () { return { type: GET_USER_REQUEST } }
  function success (user) { return { type: GET_USER_SUCCESS, user } }
  function failure (error) { return { type: GET_USER_FAILURE, error } }
}
export const updateUser = (user) => {
  return dispatch => {
    return userService.updateUser(user)
    .then(
      response => {
        dispatch(success(response.data))
        dispatch(successAlert(response.message))
        return true
      },
      error => dispatch(failure(error.toString()))
    )
  }
  function success (user) { return { type: UPDATE_USER_SUCCESS, user } }
  function failure (error) { return { type: UPDATE_USER_FAILURE, error } }
}
export const updateProfile = (user) => {
  return dispatch => {
    return userService.updateUser(user)
    .then(
      response => {
        dispatch(update(response.data))
        localStorage.setItem('user', JSON.stringify(response.data))
        dispatch(success(response.data))
        dispatch(successAlert(response.message))
        return true
      },
      error => dispatch(failure(error.toString()))
    )
  }
  function update (user) { return { type: UPDATE_PROFILE, user } }
  function success (user) { return { type: UPDATE_USER_SUCCESS, user } }
  function failure (error) { return { type: UPDATE_USER_FAILURE, error } }
}
export const deleteUser = (id) => {
  return dispatch => {
    userService.deleteUser(id)
    .then(
        response => {
          dispatch(success())
          dispatch(successAlert(response.message))
        },
        error => dispatch(failure(error.toString()))
    )
  }

  function success () { return { type: DELETE_USER_SUCCESS } }
  function failure (error) { return { type: DELETE_USER_FAILURE, error } }
}
export const sendFile = (file) => {
  return dispatch => {
    return userService.sendFile(file)
    .then(
        response => {
          dispatch(successAlert(response.message))
          return response.data
        },
        error => dispatch(errorAlert(error.toString()))
    )
  }
}
export const emptyUser = () => {
  return {
    type: EMPTY_USER
  }
}
export const getProfile = () => {
  return {
    type: GET_MY_PROFILE,
    user: JSON.parse(localStorage.getItem('user'))
  }
}
