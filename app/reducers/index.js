// @flow
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { authentication } from './authentication'

export default function createRootReducer(history: History) {
  return combineReducers({
    router: connectRouter(history),
    authentication
  });
}
